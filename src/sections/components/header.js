import React from 'react'
import {
    Image,
    StyleSheet,
    SafeAreaView,
    Text,
    View
} from 'react-native';


function Header(props){
    return(
        <View>
            <SafeAreaView>
                <View style={styles.cointainer}>
                    {/* <Image 
                        source={require('../../../assets/logo_header.png')}
                        style={styles.logo}
                    /> */}
                    <View style={styles.home}>
                        {props.children}
                    </View>
                    <Image 
                        source={require('../../../assets/filter.png')}
                        style={styles.right}
                    />
                </View>
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    logo: {
        width: 60,
        height: 30,
        resizeMode: 'contain',
        justifyContent: 'flex-start',
    },
    cointainer: {
        paddingVertical: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
    },
    home: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        fontSize: 20,
        color: '#44546b',
        resizeMode: 'contain',
        fontWeight: 'bold',
    },
    right: {
        flexDirection: 'row',
        width: 50,
        height: 25,
        resizeMode: 'contain',
        justifyContent: 'flex-end',
    }
})

export default Header;