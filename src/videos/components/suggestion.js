import React from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet
} from 'react-native';

function Suggestion(props) {
  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.cover}
          source={{
            uri: 'https://image.tmdb.org/t/p/w185/' + props.poster_path
          }}
        />
      </View>

      <View style={styles.right}>
        <Text style={styles.title}>{props.title}</Text>
        <Text>{props.release_date} | {props.original_language}</Text>
        <Text>{props.genre_ids[0]}</Text>
        <Text style={styles.average}>{props.vote_average} | {props.vote_count} votes</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  cover: {
    height: 150,
    width: 100,
    resizeMode: 'contain',
    borderRadius: 5
  },
  right: {
    paddingLeft: 10,
  },
  title: {
    fontSize: 18,
    color: '#44546b'
  },
  average: {
  }
})

export default Suggestion;