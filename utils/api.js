const BASE_API_TEST = 'https://yts.am/api/v2/';
const BASE_API = 'https://api.themoviedb.org/3/';
const PUBLIC_KEY = '31a5b1419e76b45b46d97aa8719d8100';

class Api {
  async getMostPopularMovies(id) {
    const query = await fetch(`${BASE_API}movie/popular?api_key=${PUBLIC_KEY}&language=fr`);
    const { results } = await query.json();
    return results
  }

  async getMovieTrailer(id) {
    const query = await fetch(`${BASE_API_TEST}movie/${id}/videos?api_key=${PUBLIC_KEY}`);
    const { results } = await query.json();
    return results
  }

  async getSuggestion(id) {
    const query = await fetch(`${BASE_API_TEST}movie_suggestions.json?movie_id=${id}`);
    const { data } = await query.json();
    return data.movies
  }

  async getMovies() {
    const query = await fetch(`${BASE_API_TEST}list_movies.json`);
    const { data } = await query.json();
    return data.movies
  }
}

export default new Api();